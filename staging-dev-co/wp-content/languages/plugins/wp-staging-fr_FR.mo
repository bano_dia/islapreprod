��    �                  2     #   D  ?   h  �   �  �   3  <   �          (     ;     N     l     �     �     �  �   �     i     {     �      �  )   �          
          -     G  >   O  d   �  %   �  &     5   @  2   v  4   �     �     �  3        6  -   F     t  ,   �  
   �     �     �  P   �     5     N  -   j     �     �     �     �     �     �  J     -   e     �     �  �   �     .     H     L     R     b     p     x     �  �   �     H  9   T     �  &   �  -   �  �   �     �     �  �   �     H     V     f     n  
   u     �     �     �     �     �     �     �     �     �               %     *  n   <  "   �  	   �  �   �     g  A   t     �     �  *   �     �  Z     !   a     �  ,   �     �     �     �        '   7      _      g      s      z      �      �      �      �   @   �   �   �   \   }!  �   �!     `"     i"     {"     �"  $   �"     �"  5   �"  )   	#  %   3#     Y#  	   g#  L   q#     �#     �#     �#     �#  +   �#      $     ,$     <$  �   U$     �$  �   
%  h   �%  �   6&     �&  	   �&     �&  '   �&     '     /'  3   F'     z'  7   �'     �'     �'  U   �'     )(     5(     N(     W(  
   q(     |(     �(  K   �(     �(  �   )  #   �)     *     /*     B*     V*     g*  d   z*  `   �*  4   @+  
   u+     �+  `   �+     �+  �   ,     �,     �,  �  �,  (   �.     �.  L  �.  M   E0  ,   �0  I   �0  �   
1  �   �1  Y   �2     �2     �2     3  )   #3  *   M3     x3     3     �3  �   �3     z4  .   �4  (   �4  5   �4  7   $5     \5     d5  "   |5  '   �5     �5  O   �5  �   6  ;   �6  =   �6  W   "7  1   z7  :   �7  !   �7     	8  B   8     Y8  D   w8  *   �8  6   �8     9     -9  	   H9  c   R9  (   �9  (   �9  /   :  &   8:     _:     ~:     �:  -   �:  -   �:  a   �:  G   X;     �;     �;  �   �;  '   Z<     �<     �<     �<     �<  	   �<     �<     �<  �   �<     �=  M   �=     �=  (   >  A   <>  �   ~>     `?     i?  �   �?     2@     D@     Z@  	   b@     l@     y@     �@     �@     �@      �@     �@     �@     A     A     )A     @A     VA     ]A  |   wA  /   �A     $B  �   ;B     �B  H   �B     BC     ]C  1   nC     �C  n   �C  6   D      SD  7   tD     �D     �D  ,   �D  *   E  0   DE     uE     �E     �E  5   �E     �E  	   �E  	   �E     �E  d   F  �   kF  r   *G  �   �G  	   EH     OH     fH     |H  ,   �H     �H  P   �H  6   &I  .   ]I     �I     �I  M   �I     J     !J     )J     ?J  2   GJ     zJ     �J  *   �J  �   �J     �K  �   �K  �   �L  �   +M     �M     �M     �M  )   �M  (    N      IN  :   jN     �N  `   �N     O     +O  v   CO     �O     �O     �O  $   �O  
   P     (P  #   7P  \   [P     �P  "  �P  *   �Q     R     %R     8R     MR     _R  �   yR  �   S  7   �S  
   �S     �S  r   �S  "   `T  �   �T     .U     FU  O   _U  +   �U     �U     @   �   �   7   t      y      �           �   )   H   X   I   3   "           {       <                    4   z   L   +   =   v      �   �   �   (   �       K                     �      *       �   }      Q       M   G   �   ,   s   e   ?   !   �   l   �   U   a   �   J       >       �   $       
      9       �   �   u               �   �          6   ]   1       �           �   o      |   �              [   �       �   �   �   �   �   -   �           n   S          ^       ;       �           C   .   x       R       �                   #   g          &       '          �   c   �   �   P   �   �   �   q          �   �   h   �   �      �   /   _      �   �       �      F   m   %   V   k   �   �       �       �   �   �   �   �   �   �       Z       �      E   	       Y   �      A       2   B                       b       ~       i       �   �   T   p       `   w   N   f      j      �       8   r      �   �       \       5   D   �          �   O   d   W   0                  :        - Copy Themes & Plugins from Staging to Live Site (Login with your admin credentials) <a href='%s' target='_new'>Open WP STAGING Pro on Live Site</a> <br>Probably not enough free disk space to create a staging site. <br> You can continue but its likely that the copying process will fail. <strong>Important:</strong> If CPU Load Priority is <strong>Low</strong> set a file copy limit value of 50 or higher! Otherwise file copying process takes a lot of time. A good plugin for an entire WordPress backup is the free one Access Denied Access Permissions Advanced Settings  All files will be copied to:  Allow access from all visitors BACK Back Backups By submitting, I accept the <a href="https://wp-staging.com/privacy-policy/" target="_blank">Privacy Policy</a> and consent that my email will be stored and processed for the purposes of proving support. CPU Load Priority CSS & layout broken after push Can not login to staging site Can not update WP Staging plugin Can we help? Please describe your problem Cancel Cancel Update Check Directory Size Check required disk space Cloning Cloning process is fast and does not slow down website loading Comes with our 30-day money back guarantee * You need to give us chance to resolve your issue first. Copy Staging Site to Custom Directory Copy Staging Site to Separate Database Copy plugin and theme files from staging to live site Create a clone of your website with a simple click Create a staging clone site for testing & developing Create new staging site Created on: Database Prefix: <span class="wpstg-bold">%s</span> Database Tables Database must be created manually in advance! Database tables to remove Database: <span class="wpstg-bold">%s</span> Debug Mode Delay Between Requests Delete Delete this clone. Select specific folders and database tables in the next step. Did not find a solution? Directory name is required! Directory: <span class="wpstg-bold">%s</span> Disable admin authorization Display working log Don't deactivate Edit Edit backup name and / or notes Email address is not valid. Enter one folder path per line.<br>Folders must start with absolute path:  Error: Can not connect to external database:  Export Export Settings Export the WP-Staging settings for this site as a .json file. This allows you to easily import the configuration into another site. Extra directories to copy FAQ Files Files to remove Follow @wpstg General Get WP STAGING Pro Get WP Staging Pro Go to <a href="%s" target="_blank">Settings > General</a> and make sure that WordPress Address (URL) and Site Address (URL) do both contain the same http / https scheme. High (fast) Hold on, another WP Staging process is already running... I already did I understand! (Do not show this again) I want to rate later - Ask me again in a week If your server uses rate limits it blocks requests and WP Staging can be interrupted. You can resolve that by adding one or more seconds of delay between the processing requests.  Import Import Settings Import the WP-Staging settings from a .json file. This file can be obtained by exporting the settings on another site using the form above. Import/Export Keep Permalinks License Log In Low (slow) Mail Delivery Setting Medium (average) Miss a feature More Multisite Users Only:  No, not good enough Notes: Once Weekly Once a month Only Deactivate Only temporary Open Open Staging Site Open a <a href="%s" target="_blank" rel="external nofollow">support ticket</a> and we will resolve it quickly. Open the staging site in a new tab Optimizer Optional: Submit the <a href="%s" target="_blank">System Log</a> and your WordPress debug log. This helps us to resolve your technical issues. Other reason Otherwise your staging site will not be reachable after creation. Out of disk space. Overview Page not found – Error 404 after Pushing Password Path must be writeable by PHP and an absolute path like <code>/www/public_html/dev</code>. Please accept our privacy policy. Please enter your issue. Please let us know why you are deactivating: Please read this first: Please specify, if possible Please upload a file to import Processing, please wait... Push staging site to production website Pushing Remember Me Remove Remove Data on Uninstall? Report Issue Restore Resume Scanning Select folders to copy. Click on folder name to list subfolders! Select multiple tables by pressing left mouse button and moving or by pressing STRG+Left Mouse button. (Mac ⌘+Left Mouse Button) Select the tables to copy. Tables beginning with the prefix '%s' have already been selected. Selected folder and all of its subfolders and files will be deleted. <br/>Unselect it if you want to keep the staging site file data. Settings Settings updated. Share on Facebook Sites / Start Skip WooCommerce Orders and Products Staging Site Name: Staging Site is available to authenticated users only Staging site redirects to production site Staging site returns blank white page Start Cloning Start Now Status: <span class="wpstg-bold" style="color:#ffc2c2;" title="%s">%s</span> Stop other process Submit Submit & Deactivate Success Switched to another plugin/staging solution System Info Technical Issue Test Database Connection Thanks for using <strong>WP Staging </strong> for more than 1 week.
                May I ask you to give it a <strong>5-star</strong> rating on wordpress.org? That's a Pro Feature The Optimizer is a mu plugin which disables all other plugins during WP Staging processing. Usually this makes the cloning process more reliable. If you experience issues, disable the Optimizer. This can happen if the password of the external database has been changed or if the database was deleted This staging site is located in another database and needs to be edited with <a href='https://wp-staging.com' target='_blank'>WP STAGING Pro</a> Tools Tutorial: Tweet #wpstaging URL: <span class="wpstg-bold">%s</span> Unable to open %s with mode %s Unable to write to: %s Unknown error. Please reload the page and try again Unselect All Unselect all database tables you do not want to delete: Update Update Clone Update and overwrite this clone. Select folders and database tables in the next step. Updated on: Updated: <span>%s</span> Username Username or Email Address WP STAGING WP STAGING Pro WP STAGING Pro %s is available! WP STAGING Pro allows to clone and push Multisites, (main site & sub sites) WP Staging  WP Staging Folder Permission error:</strong>
        %1$s is not write and/or readable.
        <br>
        Check if the folder <strong>%1$s</strong> is writeable by php user %2$s or www-data .
        File permissions should be chmod 755 or 777. WP Staging HTTP/HTTPS Scheme Error: WP Staging Jobs WP Staging License WP Staging Settings WP Staging Tools WP Staging Welcome WP Staging and WP STAGING Pro cannot both be active. We've automatically deactivated WP STAGING Pro. WP Staging and WP STAGING Pro cannot both be active. We've automatically deactivated WP STAGING. WP Staging is coded well for protection of your data WP-STAGING What is this? WordPress Multisite is not supported! Upgrade to <a href="%s" target="_blank">WP STAGING Pro</a> Yes, I like Your Plugin You can still delete this staging site but deleting this site will not delete any table or database. You will have to delete them manually if they exist. Your Staging Sites: Your WP STAGING Team Your version of WP STAGING has not been tested with WordPress %2$s.<br/><br/>WP STAGING has an enterprise-level quality control that performs a compatibility audit on every new WordPress release.<br/>We prioritize testing the Pro version of the plugin first, which receives the compatibility audit earlier than the Free version. If you are in a rush, upgrade to Pro today to get the latest version of WPSTAGING.<p><a href="%1$s" target="_blank"><strong>Get the latest version Now</strong></a>. https://wordpress.org/plugins/wp-staging https://wp-staging.com PO-Revision-Date: 2020-11-25 21:21:51+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - WP STAGING &#8211; Backup Duplicator &amp; Migration - Stable (latest release)
  - Copier les thèmes et les extensions du site de staging vers le site actif (Connectez vous avec vos identifiants admin) <a href='%s' target='_new'>Ouvrir WP Staging Pro sur le site en ligne</a> <br>Il semble ne pas y avoir assez d’espace libre pour créer un site de staging.<br> Vous pouvez continuer, mais il est probable que le processus de copie échoue. <strong>Important :</strong> Si la priorité de chargement CPU est <strong>basse</strong> définir une valeur limite de copie de fichier à 50 ou plus ! Sinon, le processus de copie de fichier prendra beaucoup de temps. Une bonne extension pour une sauvegarde complète de WordPress est celle qui est gratuite Accès refusé Permissions d’accès Réglages avancés  Tous les fichiers seront copiés dans :  Autoriser l’accès à tous les visiteurs RETOUR Retour Sauvegardes En envoyant, j’accepte les <a href="https://wp-staging.com/privacy-policy/" target="_blank">règles de politique de confidentialité</a> et consens à ce que mon e-mail soit stocké et traitées dans le but de prouver le support. Priorité de chargement CPU CSS et mise en page cassés après la poussée Connexion impossible au site de staging  Impossible de mettre à jour l’extension WP Staging Pouvons-nous aider ? Veuillez décrire votre problème Annuler Annuler la mise à jour Vérifier la taille du répertoire Vérifier l’espace disque nécessaire Clonage Le processus de clonage est rapide et ne ralentit pas le chargement du site Web Livré avec notre garantie de remboursement de 30 jours*. Vous devez préalablement nous donner une chance de résoudre votre problème. Copier le site de staging dans un répertoire personnalisé Copier le site de staging dans une base de données séparée Copier les fichiers des extensions et des thèmes du site de staging vers le site actif Créez un clone de votre site web en un seul clic Créez un site clone de staging pour tester et développer Créer un nouveau site de staging Créé le : Préfixe de base de données : <span class="wpstg-bold">%s</span> Tables de la base de données La base de données doit au préalable être créée manuellement ! Tables de la base de données à supprimer Base de données : <span class="wpstg-bold">%s</span> Mode débogage Délai entre les requêtes Supprimer Supprimer ce clone. Sélectionner les dossier et tables de base de données à la prochaine étape. Vous n’avez pas trouvé de solution ? Le nom du répertoire est obligatoire ! Répertoire <span class="wpstg-bold">%s</span> Désactiver l’autorisation d’admin Afficher le journal de travail Ne désactivez pas Modifier Modifier le nom de la sauvegarde ou les notes L’adresse de messagerie n’est pas valide. Saisir un chemin de dossier par ligne.<br>Les dossiers doivent commencer par un chemin absolu :  Erreur : impossible de se connecter à la base de données externe :  Exporter Exporter les réglages Exportez les Réglages WP Staging pour ce site sous forme de fichier .json. Cela vous permet d’importer facilement la configuration dans un autre site. Répertoires supplémentaires à copier FAQ Fichiers Fichiers à supprimer Suivre @wpstg Général Obtenir WP Staging Pro Obtenir WP Staging Pro Aller à <a href="%s" target="_blank">Réglages > Général</a> et assurez-vous que l’adresse WordPress (URL) et l’adresse du site (URL) respectent le même schéma http/https. Haute (rapide) Attendez, un autre processus WP Staging est déjà en cours d’exécution… Je l’ai déjà fait Je comprends ! (n’affichez plus cela) Je veux noter plus tard - me demander à nouveau dans une semaine Si votre serveur utilise des limites de débit, il bloque les demandes et WP Staging peut être interrompu. Vous pouvez résoudre ce problème en ajoutant une ou plusieurs secondes de délai entre les demandes de traitement.  Importer Importer des réglages Importez les réglages WP-Staging à partir d’un fichier .json. Ce fichier peut être obtenu en exportant les réglages sur un autre site en utilisant le formulaire ci-dessus. Importer/exporter Garder les permaliens Licence Connexion Basse (lent) Réglages des envois par e-mail Moyenne Manque une fonctionnalité Plus Comptes multisite uniquement :  Non, pas assez bon Remarques : Une fois par semaine Une fois par mois Désactiver uniquement Uniquement temporaire Ouvrir Ouvrir un site de staging Ouvrez un <a href="%s" target="_blank" rel="external nofollow">ticket de support</a> et nous allons le résoudre rapidement. Ouvrez le site de staging dans un nouvel onglet Outil d’optimisation Facultatif : Envoyer le <a href="%s" target="_blank">journal système</a> et votre journal de débogage WordPress. Cela peut nous aider à résoudre vos problèmes techniques. Autre raison Sinon, votre site de staging ne sera pas accessible après la création. Espace disque insuffisant. Vue d’ensemble Page non trouvée - Erreur 404 après la poussée Mot de passe Le chemin doit être inscriptible par PHP ainsi qu’un chemin absolu comme <code>/www/public_html/dev</code>. Veuillez accepter notre politique de confidentialité. Veuillez saisir votre problème. Veuillez nous faire savoir pourquoi vous désactivez : Veuillez lire ceci d’abord : Veuillez préciser, si possible Veuillez téléverser un fichier à importer Traitement en cours, veuillez patienter... Pousser le site de staging sur le site en ligne  Mise en place Se souvenir de moi Retirer Supprimer les données lors de la désinstallation ? Signaler un problème Restaurer Reprendre Analyse Sélectionnez les dossiers à copier. Cliquez sur le nom du dossier pour lister les sous-dossiers ! Sélectionnez plusieurs tables en appuyant sur le bouton gauche de la souris et en déplaçant ou en appuyant sur CTRL + le bouton gauche de la souris. (Mac ⌘ + bouton gauche de la souris) Sélectionner les tables à copier. Les tables commençant avec le préfixe '%s' ont déjà été sélectionnées. Le dossier sélectionné et tous ses sous-dossiers et fichiers seront supprimés. <br/>Désélectionnez-le si vous souhaitez conserver les données du site de staging. Réglages Réglages mis à jour. Partager sur Facebook Sites / Démarrer Passer les commandes et produits WooCommerce Nom du site de staging : Le site de staging est uniquement disponible pour les utilisateurs authentifiés Redirection du site de test vers le site de production Le site de staging a renvoyé une page blanche Démarrer le clonage Commencer maintenant État : <span class="wpstg-bold" style="color:#ffc2c2;" title="%s">%s</span> Arrêter l’autre processus Envoyer Envoyer & désactiver Réussi Basculé vers une autre extension/solution de test Information système Problème technique Tester la connexion à la base de données  Génial, vous utilisez <strong>WP Staging </strong> depuis plus d’une semaine.
        Puis-je vous demander de lui donner une note de <strong>5 étoiles</strong> sur WordPress.org ? C’est une fonctionnalité Pro L’optimiseur est une extension indispensable qui désactive toutes les autres extensions lors du traitement de WP Staging. Cela rend généralement le processus de clonage plus fiable. Si vous rencontrez des problèmes, désactivez l’optimiseur. Cela peut arriver lorsque le mot de passe de la base de données externe a été changé ou si la base de données à été supprimée. Ce site de staging est situé dans un autre espace de travail et doit être édité avec <a href=’https://wp-staging.com’ target=’_blank’>WP Staging Pro</a> Outils Tutoriel : Tweeter #wpstaging URL : <span class="wpstg-bold">%s</span> Impossible d’ouvrir %s avec le mode %s Impossible d’écrire sur : %s Erreur inconnue. Veuillez recharger la page et réessayer. Tout désélectionner Désélectionnez toutes les tables de la base de données que vous ne souhaitez pas supprimer : Mettre à jour Mettre à jour le clone Mettre à jour et surcharger ce clone. Sélectionner les dossier et tables de base de données à la prochaine étape. Mise à jour le : Mis à jour : <span>%s</span> Identifiant Identifiant ou adresse de messagerie WP STAGING WP STAGING Pro WP Staging Pro %s est disponible ! WP Staging Pro permet de cloner et de pousser des multisites, (site principal et sous-sites) WP Staging  Erreur de droit de dossier WP Staging :</strong>
        %1$s n’est pas inscriptible et/ou lisible.
        <br>
        Vérifiez si le dossier <strong>%1$s</strong> est inscriptible par l’utilisateur PHP %2$s ou www-data.
        Les droits du fichier doivent être chmod 755 ou 777. Erreur de schéma HTTP/HTTPS WP Staging : Tâches WP Staging Licence WP Staging Réglages WP Staging Outils WP Staging Bienvenue dans WP Staging Les extensions WP Staging et WP Staging Pro ne peuvent pas être toutes deux actives. Nous avons automatiquement désactivé WP Staging Pro. Les extensions WP Staging et WP Staging PRO ne peuvent pas être toutes deux actives. Nous avons automatiquement désactivé WP Staging. WP Staging est codé pour la protection de vos données WP-STAGING Qu’est-ce que c’est ? WordPress Multisite n’est pas supporté ! Mettre à niveau vers <a href="%s" target="_blank">WP Staging PRO</a> Oui, j’apprécie votre extension Vous pouvez tout de même supprimer ce site de staging mais cela ne supprimera aucune table ou base de données. Vous devrez les supprimer manuellement si elles existent. Vos sites de staging : Votre équipe WP STAGING Votre version de WP STAGING n’a pas encore été testée avec WordPress %2$s. https://fr.wordpress.org/plugins/wp-staging https://wp-staging.com 