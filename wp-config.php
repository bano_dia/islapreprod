<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', "islapreprod");

/** MySQL database username */
define('DB_USER', "root");

/** MySQL database password */
define('DB_PASSWORD', "");

/** MySQL hostname */
define('DB_HOST', "localhost");

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'KsFONiGjK6orxAvycsUhG2op+hXzJ/JWCAHg1hnua0KP2QjTJyZzBUs5uz9g');
define('SECURE_AUTH_KEY',  'DK49cZRoP9IjBfYh5DWBndlX2rH66Bz0oH3BGQafOr0osH6/02IkSWbMxrHH');
define('LOGGED_IN_KEY',    'x9fXqMbzIKJy10ktquLg8mbzkyxQsCmYfcwPpuTb3UZUWltR9zncyCTfmAuj');
define('NONCE_KEY',        'y5vhfMzCwAFB7LhzL1bx/8v+bGbejsNY3yzaa6tjQv02gUzbC5RuhzSgRvsQ');
define('AUTH_SALT',        'HZxADdhzTEjsMWHlGOpKnQ0KUdgp0E+l077UVqOhwSy4LytjToaCC6wTupZ5');
define('SECURE_AUTH_SALT', 'xbFKmjBW6+g7IgE4Ghr1faEsOOVVc+DPDneB4Cjw0XqC4dYUSFaic4mkvLzC');
define('LOGGED_IN_SALT',   'jI1KfJKKEBQvuihZhDE4aofgRErHaTfO2gKA3TzRkGzGmHzSkyno1bAEKOU9');
define('NONCE_SALT',       'PTdtklJhlZNEQi0NwMIEYG73HXUhONT6cfT1yJojdlLg6LCNzcebWBnW59NW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'preprod_';
//$table_prefix  = 'wpstg0_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/* Fixes "Add media button not working", see http://www.carnfieldwebdesign.co.uk/blog/wordpress-fix-add-media-button-not-working/ */
define('CONCATENATE_SCRIPTS', false );

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

//define( 'WP_HOME', 'https://www.dev.islacollective.co' );
//define( 'WP_SITEURL', 'https://www.dev.islacollective.co' );

define('FS_METHOD', 'direct');
