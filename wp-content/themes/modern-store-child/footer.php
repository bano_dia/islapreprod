<?php do_action( 'ct_modern_store_main_bottom' ); ?>
<?php ct_modern_store_pagination(); ?>
</section> <!-- .main-container -->
<?php do_action( 'ct_modern_store_after_main' ); ?>

<?php 
// Elementor `footer` location
if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) :
?>
<footer id="site-footer" class="site-footer" role="contentinfo">
    <?php do_action( 'ct_modern_store_footer_top' ); ?>
     <!-- Code below was intended to show socialmedia but was not working
    <div class="design-credit">
        <div class="social-media-footer-box">
            <div id="social-icons-container" class="social-icons-container">
                        <?php ct_modern_store_social_icons_output(); ?>
                    </div>
        </div>
        <a href="http://www.islacollective.co/sign-up-to-our-newletter/">Sign up to our Newsletter to have exclusive first-dibs access to our products</a>
    </div> -->
    <div class="design-credit" style="display: flex; margin: 1em;">
        <div class="social-media-footer-box">
            <div id="social-icons-container" class="social-icons-container">
                        <ul class="social-media-icons">

                            <li>
                                <a class="facebook" target="_blank" href="https://www.facebook.com/islacollective2020/">
                                    <i class="fab fa-facebook" title="facebook"></i>
                                </a>
                            </li>

                            <li>

                                <a class="instagram" target="_blank" href="https://www.instagram.com/islacollective__/">
                                <i class="fab fa-instagram" title="instagram"></i>
                                </a>


                             </li>


                        </ul>                  
            </div>
        </div>
        <div class="footerlink" style="margin: 0 auto;">
        	<span style="font-size: 3em;"><a href="https://islacollective.co/are-you-looking-for-the-pot-of-gold/">🌈</a></span>
    	</div>
    </div>
</footer>
<?php endif; ?>
</div><!-- .max-width -->
</div><!-- .overflow-container -->

<?php do_action( 'ct_modern_store_body_bottom' ); ?>

<?php wp_footer(); ?>

</body>
</html>