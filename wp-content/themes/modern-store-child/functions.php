<?php
//funcitn to solve SSL issue as per suggested by OVH 21/10/2020
function add_cors_http_header(){
header("Access-Control-Allow-Origin: *");
}
add_action('init','add_cors_http_header');

// function to enquee child theme style sheet after parent
function wcmp_shop_page() {

    
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    wp_enqueue_style('child-style', get_stylesheet_directory_uri() .'/style.css' ,  array('parent-style'));
    
}
add_action( 'wp_enqueue_scripts', 'wcmp_shop_page');

add_filter('wcmp_vendor_list_enable_store_locator_map', '__return_false');

add_filter('rest_url', function($url) {
    $url = str_replace(home_url(), site_url(), $url);
    return $url;
});

/**
** Hide vendor details form vendor shop page. FYI for the vendor list i just removed it from the html code
**/
add_filter('wcmp_vendor_store_header_hide_store_phone', '__return_true');
add_filter('wcmp_vendor_store_header_hide_store_email', '__return_true');
add_filter('wcmp_vendor_store_header_hide_store_address', '__return_true');



/**
** Hide vendors without products on wcmp vendor list page
**/
add_filter('wcmp_vendor_list_get_wcmp_vendors_args', 'hide_zero_product_vendor_on_vendor_list_page' , 10 , 4 );
function hide_zero_product_vendor_on_vendor_list_page ( $query, $order_by, $request, $atts ) {
   $vendor = get_wcmp_vendors();
   $vendor_ids = wp_list_pluck($vendor , 'id');
   foreach ($vendor_ids as $key => $value) {
      $vendor = get_wcmp_vendor($value);
      $vendor_products = $vendor->get_products();
      if (empty($vendor_products)){
          $get_vendor[] = $value;
       }
   }
   $query['exclude'] = $get_vendor;
   return $query;
}

// change text of "sold by"
add_filter( 'wcmp_sold_by_text', 'filter_wcmp_sold_by_text' );
function filter_wcmp_sold_by_text() {
  $sold_by_text = 'By';
  return $sold_by_text;
}


//Shipping for Laura

/* Disable autocreate vendor shipping class */
add_filter('wcmp_add_vendor_shipping_class', '__return_false');

/* Allow vendors to view/choose admin created shipping classes */
add_filter('wcmp_allowed_only_vendor_shipping_class', '__return_false');	

add_action('init', 'init_wcmp');
function init_wcmp(){
   global $WCMp;
   remove_filter('woocommerce_product_options_shipping', array($WCMp->vendor_dashboard, 'wcmp_product_options_shipping'), 5);
}

//ISLA: edit on backend dashboard
//avoid seeing sales data from vendor backend dashbaord
add_action( 'admin_init', function() {
      if (is_user_wcmp_vendor(get_current_user_id())) {
        remove_menu_page( 'wc-reports' );
      }
    } );

//ISLA: show "out of stock" from product listing page
add_action( 'woocommerce_after_shop_loop_item', 'bbloomer_display_sold_out_loop_woocommerce', 7 );
 
function bbloomer_display_sold_out_loop_woocommerce() {
    global $product;
    if ( !$product->is_in_stock() ) {
      ?>
      <p style="display: block;margin: 0;font-weight: bold;color: #6A04FF;">SOLD OUT</p>
      <p style="display: block;margin: 0;font-weight: bold;color: #e564e0;">Click to ask for more</p>
        <?php
    }
}

//ISLA: EDIT PRODUCT PAGE
//add q and A section to product summary
function my_custom_action() { 
    ?>
    <br>
    <div id="wcmp_customer_qna" class="woocommerce-wcmp_customer_qna">
    <div id="cust_qna_form_wrapper">
        <div id="cust_qna_form">
            <h2 id="custqna-title" class="custqna-title">
              <span style='font-weight: bold; color: #F1419A;'>Ask </span>
              <span style='font-weight: bold; color: #690AFF;'>for </span>
              <span style='font-weight: bold; color: #ffbf2c'>your </span>
              <span style='font-weight: bold; color: #0CC1CB'>customizations </span>
              <span style='font-weight: bold; color: #F1419A;'>here</span>
            </h2>     
            <p style= "margin: 1em 0; font-size: 14px";> 
              <span style='color: black'>Not your size or the colour you'd like? No problem! Ask for a customization to get your </span>
              <span style='font-weight: bold; color: #F1419A;'>U</span>
              <span style='font-weight: bold; color: #690AFF;'>N</span>
              <span style='font-weight: bold; color: #ffbf2c'>I</span>
              <span style='font-weight: bold; color: #0CC1CB'>Q</span>
              <span style='font-weight: bold; color: #F1419A'>U</span>
              <span style='font-weight: bold; color: #690AFF'>E</span>
              <span style='color: black'>version of this product.</span>
            </p>
            <p style= "margin: 1em 0; font-size: 12px";> You need to be signed in. Log in or create your user.</p>
            <div class="qna-ask-wrap">
                <form action="" method="post" id="customerqnaform" class="customerqna-form" novalidate="">
                    <input type="hidden" id="cust_qna_nonce" name="cust_qna_nonce" value="cb23a9b72a"><input type="hidden" name="_wp_http_referer" value="/product/upcycled-mask/">                    <div id="qna-ask-input">
                        <input type="text" name="cust_question" id="cust_question" placeholder="Have a question? Search for answer">
                        <div id="ask-wrap">
                            <label class="no-answer-lbl">Haven't found any answer you are looking for</label>
                            <button id="ask-qna" class="btn btn-info btn-lg" type="button">Ask Now</button>
                        </div>
                        <input type="hidden" name="product_ID" value="15" id="product_ID">
                        <input type="hidden" name="cust_ID" id="cust_ID" value="1">
                    </div>
                </form> 
            </div>
            <div id="qna-result-wrap" class="qna-result-wrap">
                        </div>
            <div class="clear"></div>
                    </div>
    </div>
</div> 
<?php
;
};     
add_action( 'woocommerce_single_product_summary', 'my_custom_action', 35 ); 

/**
 * ISLA: Remove product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['wcmp_customer_qna'] );        // Questions and answers

    return $tabs;
}


//ISLA: edit on shop page
// ad a description of the ISLA products
add_action( 'woocommerce_before_shop_loop', 'subtitleproducts', 7 );
 
function subtitleproducts() {
      ?>
      <p style="display: block;margin: 1em 0;">All products on ISLA are one-of-a-kind and upcycled. Upcycled means they are made by reworking existent second-hand clothes, giving them a new life, or by using textile scraps that would have otherwise ended up in landfill.</p>
        <?php
    }



?>